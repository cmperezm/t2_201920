package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			if(option==1)
			{
				modelo.cargar();
			}
			else if(option==2)
			{
				System.out.println("Ingrese Hora: ");
				double hour = lector.nextDouble();
				modelo.hourSort(hour);
			}
			else if(option==3)
			{
				System.out.println("Ingrese el numero de viajes y y hora separado por una , ");
				String dat2 = lector.next();
				String[] data = dat2.split(",");
				modelo.procesarPila(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
			}		
			else if(option==4) 
			{
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
			}

			else{

				System.out.println("--------- \n Opcion Invalida !! \n---------");

			}
		}

	}	
}
