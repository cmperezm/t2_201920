package model.data_structures;

public interface IQueue<E> extends Iterable<E>{

	public void enqueue(E element);
	
	public E dequeue();
	
	public int size();
}
